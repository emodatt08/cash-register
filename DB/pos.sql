/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100129
 Source Host           : localhost:3306
 Source Schema         : pos

 Target Server Type    : MySQL
 Target Server Version : 100129
 File Encoding         : 65001

 Date: 07/02/2021 19:37:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  INDEX `ci_sessions_timestamp`(`timestamp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('udngsf5q74v9i89bibda4jpggoeq8ngm', '::1', 1612720029, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732303032393B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B737563636573737C733A33383A22537563636573732121204E6577204974656D204164646564205375636365737366756C6C7921223B5F5F63695F766172737C613A313A7B733A373A2273756363657373223B733A333A226E6577223B7D);
INSERT INTO `ci_sessions` VALUES ('lsmmn1q7g24c5asqp83meeu8fhtgtpha', '::1', 1612721031, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732313033313B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('l27qpm4tm4t3ua4lm0sdosp4bkl1id07', '::1', 1612722447, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732323434373B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('kj1e10v4542ej2vu41m9bkd1fh89uip2', '::1', 1612724511, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732343531313B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('8eiefh5at6d8vubi77e9pgn91j1uelti', '::1', 1612725345, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732353334353B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('biiikdp65rt1388rsk1ubeqn4thf4pdk', '::1', 1612725679, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732353637393B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('gq476prsl0tckn2gkga6gfnckvs8dul3', '::1', 1612726125, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732363132353B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('b2pmh7or927q6219119ekii8vsvq4gab', '::1', 1612726438, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732363433383B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);
INSERT INTO `ci_sessions` VALUES ('t7polbbp7roknqgsjgb06n367phtv7eo', '::1', 1612726565, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323732363433383B63757272656E63797C733A333A22474853223B63757272656E63795F706C6163656D656E747C733A343A224C656674223B63757272656E63795F636F64657C733A333A22474853223B766965775F646174657C733A353A22592D6D2D64223B766965775F74696D657C733A323A223234223B696E765F757365726E616D657C733A393A22656D6F646174743038223B696E765F7573657269647C733A313A2231223B6C6F676765645F696E7C623A313B726F6C655F69647C733A313A2231223B726F6C655F6E616D657C733A353A2261646D696E223B6C616E67756167657C733A373A22456E676C697368223B6C616E67756167655F69647C733A313A2231223B);

-- ----------------------------
-- Table structure for db_brands
-- ----------------------------
DROP TABLE IF EXISTS `db_brands`;
CREATE TABLE `db_brands`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_code` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `brand_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_brands
-- ----------------------------
INSERT INTO `db_brands` VALUES (1, 'CT0001', 'Vodka', 'Vodka is a clear distilled alcoholic beverage with different varieties originating in Poland, Russia', '1');

-- ----------------------------
-- Table structure for db_category
-- ----------------------------
DROP TABLE IF EXISTS `db_category`;
CREATE TABLE `db_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_category
-- ----------------------------
INSERT INTO `db_category` VALUES (1, 'CT0001', 'Cocktails', 'A cocktail is an alcoholic mixed drink, which is either a combination of spirits, or one or more spi', '1');

-- ----------------------------
-- Table structure for db_cobpayments
-- ----------------------------
DROP TABLE IF EXISTS `db_cobpayments`;
CREATE TABLE `db_cobpayments`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `payment` decimal(9, 2) NULL DEFAULT NULL,
  `payment_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_note` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `status` enum('0','1`') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_company
-- ----------------------------
DROP TABLE IF EXISTS `db_company`;
CREATE TABLE `db_company`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company_website` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` int(15) NULL DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `state` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `postcode` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gst_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `vat_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `website` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pan_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bank_details` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `supplier_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `item_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_init` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `upi_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company_logo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `upi_logo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sms_status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_country
-- ----------------------------
DROP TABLE IF EXISTS `db_country`;
CREATE TABLE `db_country`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `country` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_code` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_country
-- ----------------------------
INSERT INTO `db_country` VALUES (1, 'Ghana', 'GH', '1');

-- ----------------------------
-- Table structure for db_currency
-- ----------------------------
DROP TABLE IF EXISTS `db_currency`;
CREATE TABLE `db_currency`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency_placement` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_currency
-- ----------------------------
INSERT INTO `db_currency` VALUES (1, 'GHS', 'Ghana cedi', 'Left', 'GHS', '0');

-- ----------------------------
-- Table structure for db_customers
-- ----------------------------
DROP TABLE IF EXISTS `db_customers`;
CREATE TABLE `db_customers`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `customer_code` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `state_id` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `city` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `postcode` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_due` decimal(9, 2) NULL DEFAULT NULL,
  `address` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gstin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_number` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_return_due` date NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `opening_balance` decimal(9, 2) NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_code` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_customers
-- ----------------------------
INSERT INTO `db_customers` VALUES (1, 'Hillary Kollan', 'CR-0001', '0241763214', '0241763214', 'emodatt08@gmail.com', '1', '1', 'Adenta', '233', NULL, 'P.O.Box AH 445, Adenta-Accra', 'NULL', 'P038477477', NULL, '2021-01-24', '04:19:01', 'emodatt08', 670.00, '::1', 'MSI', NULL, '0');
INSERT INTO `db_customers` VALUES (2, 'James Lomotey', 'CR-0002', '0246778467', '0246778467', 'emodatt08@gmail.com', '1', '1', 'Madina', '233', NULL, 'P.O.Box MD 234,  Madina-Accra', 'NULL', 'P34487577', NULL, '2021-01-24', '04:31:20', 'emodatt08', 390.00, '::1', 'MSI', NULL, '1');
INSERT INTO `db_customers` VALUES (3, 'Benson Yedooah', 'CR-0003', '02417736654', '02017736654', 'emodat08@gmail.com', '1', NULL, 'Adenta', '233', NULL, 'P.O.Box AH 445,  Adenta-Accra', 'NULL', 'P099373545', NULL, '2021-01-24', '06:30:00', 'emodatt08', 100.00, '::1', 'MSI', NULL, '0');

-- ----------------------------
-- Table structure for db_expense
-- ----------------------------
DROP TABLE IF EXISTS `db_expense`;
CREATE TABLE `db_expense`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_id` int(10) NULL DEFAULT NULL,
  `expense_for` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `expense_amt` decimal(9, 2) NULL DEFAULT NULL,
  `reference_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `expense_date` date NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_expense_category
-- ----------------------------
DROP TABLE IF EXISTS `db_expense_category`;
CREATE TABLE `db_expense_category`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_hold
-- ----------------------------
DROP TABLE IF EXISTS `db_hold`;
CREATE TABLE `db_hold`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_date` date NULL DEFAULT NULL,
  `sales_status` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `customer_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `discount_to_all_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_to_all_type` decimal(10, 4) NULL DEFAULT NULL,
  `tot_discount_to_all_amt` decimal(10, 4) NULL DEFAULT NULL,
  `subtotal` decimal(9, 2) NULL DEFAULT NULL,
  `round_off` decimal(9, 2) NULL DEFAULT NULL,
  `grand_total` decimal(9, 2) NULL DEFAULT NULL,
  `pos` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_note` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_charges_amt` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_holditems
-- ----------------------------
DROP TABLE IF EXISTS `db_holditems`;
CREATE TABLE `db_holditems`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hold_id` int(11) NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_qty` decimal(10, 4) NULL DEFAULT NULL,
  `price_per_unit` decimal(9, 2) NULL DEFAULT NULL,
  `tax_id` int(11) NULL DEFAULT NULL,
  `tax_amt` decimal(9, 2) NULL DEFAULT NULL,
  `tax_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `discount_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `discount_input` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `discount_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_init
-- ----------------------------
DROP TABLE IF EXISTS `db_init`;
CREATE TABLE `db_init`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_init` int(11) NULL DEFAULT NULL,
  `item_init` int(11) NULL DEFAULT NULL,
  `supplier_init` date NULL DEFAULT NULL,
  `purchase_init` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_return_init` decimal(9, 2) NULL DEFAULT NULL,
  `customer_init` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_init` date NULL DEFAULT NULL,
  `time_format` time(0) NULL DEFAULT NULL,
  `sales_discount` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_return_init` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `expense_init` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_invoice_footer_text` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_terms_and_conditions` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_items
-- ----------------------------
DROP TABLE IF EXISTS `db_items`;
CREATE TABLE `db_items`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `item_image` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_price` int(10) NULL DEFAULT NULL,
  `brand_id` int(11) NULL DEFAULT NULL,
  `unit_id` int(11) NULL DEFAULT NULL,
  `lot_number` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_price` decimal(10, 2) NULL DEFAULT NULL,
  `custom_barcode` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hsn` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sku` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `stock` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_type` decimal(10, 4) NULL DEFAULT NULL,
  `profit_margin` decimal(9, 2) NULL DEFAULT NULL,
  `item_code` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `system_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `expire_date` date NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `alert_qty` decimal(10, 4) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_items
-- ----------------------------
INSERT INTO `db_items` VALUES (1, 'Sex on the beach', 'uploads/items/1612720029.jpg', 'A Sex on the Beach is an alcoholic cockt', 24, 1, 1, '49495', 23.00, '344757', '45885', '3457', '23', '1', '30', 0.0000, 3.00, '0001', '::1', '2021-02-07', '05:47:09', 'MSI', 'emodatt08', '1970-01-01', 1, 1.0000, '1');
INSERT INTO `db_items` VALUES (2, 'Long Island iced tea', 'uploads/items/1612720202.jpg', 'A Long Island iced tea is a type of alco', 12, 1, 1, '49491', 12.00, '344753', '45885', '3457', '12', '1', '30', 0.0000, -3.00, '0002', '::1', '2021-02-07', '05:50:02', 'MSI', 'emodatt08', '1970-01-01', 1, 30.0000, '1');

-- ----------------------------
-- Table structure for db_languages
-- ----------------------------
DROP TABLE IF EXISTS `db_languages`;
CREATE TABLE `db_languages`  (
  `language_id` int(10) NOT NULL AUTO_INCREMENT,
  `language` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`language_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_languages
-- ----------------------------
INSERT INTO `db_languages` VALUES (1, 'English', '1');

-- ----------------------------
-- Table structure for db_paymenttypes
-- ----------------------------
DROP TABLE IF EXISTS `db_paymenttypes`;
CREATE TABLE `db_paymenttypes`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_paymenttypes
-- ----------------------------
INSERT INTO `db_paymenttypes` VALUES (1, 'Cash', '1');
INSERT INTO `db_paymenttypes` VALUES (2, 'Cheque', '1');
INSERT INTO `db_paymenttypes` VALUES (3, 'MTN Mobile Money', '1');
INSERT INTO `db_paymenttypes` VALUES (4, 'Vodafone Cash', '1');
INSERT INTO `db_paymenttypes` VALUES (5, 'AirtelTigo Cash', '1');
INSERT INTO `db_paymenttypes` VALUES (6, 'Visa Debit Card', '1');
INSERT INTO `db_paymenttypes` VALUES (7, 'MasterCard Debit Car', '1');

-- ----------------------------
-- Table structure for db_permissions
-- ----------------------------
DROP TABLE IF EXISTS `db_permissions`;
CREATE TABLE `db_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `permissions` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_permissions
-- ----------------------------
INSERT INTO `db_permissions` VALUES (1, 1, 'sales_report', '1');
INSERT INTO `db_permissions` VALUES (2, 1, 'item_sales_report', '1');
INSERT INTO `db_permissions` VALUES (3, 1, 'sales_view', '1');
INSERT INTO `db_permissions` VALUES (4, 1, 'sales_return_view', '1');
INSERT INTO `db_permissions` VALUES (5, 1, 'sales_return_add', '1');
INSERT INTO `db_permissions` VALUES (6, 1, 'sales_payments_report', '1');
INSERT INTO `db_permissions` VALUES (7, 1, 'sales_return_report', '1');
INSERT INTO `db_permissions` VALUES (8, 1, 'import_customers', '1');
INSERT INTO `db_permissions` VALUES (9, 1, 'customers_view', '1');
INSERT INTO `db_permissions` VALUES (10, 1, 'customers_add', '1');
INSERT INTO `db_permissions` VALUES (11, 1, 'purchase_view', '1');
INSERT INTO `db_permissions` VALUES (12, 1, 'purchase_report', '1');
INSERT INTO `db_permissions` VALUES (13, 1, 'purchase_return_report', '1');
INSERT INTO `db_permissions` VALUES (14, 1, 'purchase_payments_report', '1');
INSERT INTO `db_permissions` VALUES (15, 1, 'purchase_return_view', '1');
INSERT INTO `db_permissions` VALUES (16, 1, 'new_purchase_return', '1');
INSERT INTO `db_permissions` VALUES (17, 1, 'purchase_add', '1');
INSERT INTO `db_permissions` VALUES (18, 1, 'purchase_view', '1');
INSERT INTO `db_permissions` VALUES (19, 1, 'purchase_return_view', '1');
INSERT INTO `db_permissions` VALUES (20, 1, 'suppliers_view', '1');
INSERT INTO `db_permissions` VALUES (21, 1, 'import_suppliers', '1');
INSERT INTO `db_permissions` VALUES (22, 1, 'suppliers_add', '1');
INSERT INTO `db_permissions` VALUES (23, 1, 'items_add', '1');
INSERT INTO `db_permissions` VALUES (24, 1, 'items_view', '1');
INSERT INTO `db_permissions` VALUES (25, 1, 'import_items', '1');
INSERT INTO `db_permissions` VALUES (26, 1, 'items_category_add', '1');
INSERT INTO `db_permissions` VALUES (27, 1, 'items_category_view', '1');
INSERT INTO `db_permissions` VALUES (28, 1, 'expired_items_report ', '1');
INSERT INTO `db_permissions` VALUES (29, 1, 'item_sales_report ', '1');
INSERT INTO `db_permissions` VALUES (30, 1, 'brand_add', '1');
INSERT INTO `db_permissions` VALUES (31, 1, 'brand_view', '1');
INSERT INTO `db_permissions` VALUES (32, 1, 'print_labels', '1');
INSERT INTO `db_permissions` VALUES (33, 1, 'expense_add', '1');
INSERT INTO `db_permissions` VALUES (34, 1, 'expense_view', '1');
INSERT INTO `db_permissions` VALUES (35, 1, 'expense_report', '1');
INSERT INTO `db_permissions` VALUES (36, 1, 'expense_category_add', '1');
INSERT INTO `db_permissions` VALUES (37, 1, 'expense_category_view', '1');
INSERT INTO `db_permissions` VALUES (38, 1, 'print_labels', '1');
INSERT INTO `db_permissions` VALUES (39, 1, 'places_add', '1');
INSERT INTO `db_permissions` VALUES (40, 1, 'places_view', '1');
INSERT INTO `db_permissions` VALUES (41, 1, 'profit_report', '1');
INSERT INTO `db_permissions` VALUES (42, 1, 'stock_report', '1');
INSERT INTO `db_permissions` VALUES (43, 1, 'users_add', '1');
INSERT INTO `db_permissions` VALUES (44, 1, 'users_view', '1');
INSERT INTO `db_permissions` VALUES (45, 1, 'roles_view', '1');
INSERT INTO `db_permissions` VALUES (46, 1, 'send_sms', '1');
INSERT INTO `db_permissions` VALUES (47, 1, 'sms_template_view', '1');
INSERT INTO `db_permissions` VALUES (48, 1, 'sms_api_view', '1');
INSERT INTO `db_permissions` VALUES (49, 1, 'company_edit', '1');
INSERT INTO `db_permissions` VALUES (50, 1, 'site_edit', '1');
INSERT INTO `db_permissions` VALUES (51, 1, 'tax_view', '1');
INSERT INTO `db_permissions` VALUES (52, 1, 'units_view', '1');
INSERT INTO `db_permissions` VALUES (53, 1, 'payment_types_view', '1');
INSERT INTO `db_permissions` VALUES (54, 1, 'database_backup', '1');
INSERT INTO `db_permissions` VALUES (55, 2, 'sales_report', '1');
INSERT INTO `db_permissions` VALUES (56, 2, 'item_sales_report', '1');
INSERT INTO `db_permissions` VALUES (57, 2, 'sales_view', '1');
INSERT INTO `db_permissions` VALUES (58, 2, 'sales_return_view', '1');
INSERT INTO `db_permissions` VALUES (59, 2, 'sales_return_add', '1');
INSERT INTO `db_permissions` VALUES (60, 2, 'sales_payments_report', '1');
INSERT INTO `db_permissions` VALUES (61, 2, 'sales_return_report', '1');
INSERT INTO `db_permissions` VALUES (62, 2, 'items_add', '1');
INSERT INTO `db_permissions` VALUES (63, 2, 'items_view', '1');
INSERT INTO `db_permissions` VALUES (64, 2, 'import_items', '1');
INSERT INTO `db_permissions` VALUES (65, 2, 'items_category_add', '1');
INSERT INTO `db_permissions` VALUES (66, 2, 'items_category_view', '1');
INSERT INTO `db_permissions` VALUES (67, 2, 'expired_items_report ', '1');
INSERT INTO `db_permissions` VALUES (68, 2, 'item_sales_report ', '1');
INSERT INTO `db_permissions` VALUES (69, 3, 'purchase_view', '1');
INSERT INTO `db_permissions` VALUES (70, 3, 'purchase_report', '1');
INSERT INTO `db_permissions` VALUES (71, 3, 'purchase_return_report', '1');
INSERT INTO `db_permissions` VALUES (72, 3, 'purchase_payments_report', '1');
INSERT INTO `db_permissions` VALUES (73, 3, 'purchase_return_view', '1');
INSERT INTO `db_permissions` VALUES (74, 3, 'new_purchase_return', '1');
INSERT INTO `db_permissions` VALUES (75, 3, 'purchase_add', '1');
INSERT INTO `db_permissions` VALUES (76, 1, 'dashboard_view', '1');

-- ----------------------------
-- Table structure for db_purchase
-- ----------------------------
DROP TABLE IF EXISTS `db_purchase`;
CREATE TABLE `db_purchase`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_code` int(10) NULL DEFAULT NULL,
  `reference_no` date NULL DEFAULT NULL,
  `purchase_date` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_status` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `supplier_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_charges_input` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_charges_tax_id` int(20) NULL DEFAULT NULL,
  `other_charges_amt` decimal(9, 2) NULL DEFAULT NULL,
  `discount_to_all_input` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `discount_to_all_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tot_discount_to_all_amt` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `subtotal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `round_off` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `grand_total` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paid_amount` decimal(9, 2) NULL DEFAULT NULL,
  `purchase_note` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_status` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `return_bit` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_purchaseitems
-- ----------------------------
DROP TABLE IF EXISTS `db_purchaseitems`;
CREATE TABLE `db_purchaseitems`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) NULL DEFAULT NULL,
  `item_id` int(10) NULL DEFAULT NULL,
  `purchase_qty` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_status` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price_per_unit` decimal(9, 2) NULL DEFAULT NULL,
  `tax_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_amt` decimal(9, 2) NULL DEFAULT NULL,
  `tax_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `unit_discount_per` decimal(10, 4) NULL DEFAULT NULL,
  `discount_amt` decimal(9, 2) NULL DEFAULT NULL,
  `tot_discount_to_all_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_purchaseitemsreturn
-- ----------------------------
DROP TABLE IF EXISTS `db_purchaseitemsreturn`;
CREATE TABLE `db_purchaseitemsreturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NULL DEFAULT NULL,
  `return_id` int(11) NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `return_qty` decimal(10, 4) NULL DEFAULT NULL,
  `return_status` int(3) NULL DEFAULT NULL,
  `price_per_unit` decimal(9, 2) NULL DEFAULT NULL,
  `tax_id` int(10) NULL DEFAULT NULL,
  `tax_type` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_discount_per` decimal(10, 4) NULL DEFAULT NULL,
  `discount_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_purchasepayments
-- ----------------------------
DROP TABLE IF EXISTS `db_purchasepayments`;
CREATE TABLE `db_purchasepayments`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  `payment_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_note` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_purchasepaymentsreturn
-- ----------------------------
DROP TABLE IF EXISTS `db_purchasepaymentsreturn`;
CREATE TABLE `db_purchasepaymentsreturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NULL DEFAULT NULL,
  `return_id` int(11) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  `payment_type` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment` decimal(9, 2) NULL DEFAULT NULL,
  `payment_note` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_purchasereturn
-- ----------------------------
DROP TABLE IF EXISTS `db_purchasereturn`;
CREATE TABLE `db_purchasereturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NULL DEFAULT NULL,
  `return_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reference_no` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `return_status` int(3) NULL DEFAULT NULL,
  `supplier_id` int(11) NULL DEFAULT NULL,
  `other_charges_input` decimal(9, 2) NULL DEFAULT NULL,
  `other_charges_tax_id` int(11) NULL DEFAULT NULL,
  `other_charges_amt` decimal(9, 2) NULL DEFAULT NULL,
  `discount_to_all_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_to_all_type` decimal(10, 4) NULL DEFAULT NULL,
  `tot_discount_to_all_amt` decimal(10, 4) NULL DEFAULT NULL,
  `subtotal` decimal(9, 2) NULL DEFAULT NULL,
  `round_off` decimal(9, 2) NULL DEFAULT NULL,
  `grand_total` decimal(9, 2) NULL DEFAULT NULL,
  `return_note` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_roles
-- ----------------------------
DROP TABLE IF EXISTS `db_roles`;
CREATE TABLE `db_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_roles
-- ----------------------------
INSERT INTO `db_roles` VALUES (1, 'admin', '1');
INSERT INTO `db_roles` VALUES (2, 'sales', '1');
INSERT INTO `db_roles` VALUES (3, 'purchase', '1');

-- ----------------------------
-- Table structure for db_sales
-- ----------------------------
DROP TABLE IF EXISTS `db_sales`;
CREATE TABLE `db_sales`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other_charges_amt` decimal(9, 2) NULL DEFAULT NULL,
  `sales_code` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reference_no` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_date` date NULL DEFAULT NULL,
  `sales_status` int(4) NULL DEFAULT NULL,
  `customer_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_charges_input` decimal(9, 2) NULL DEFAULT NULL,
  `other_charges_tax_id` int(5) NULL DEFAULT NULL,
  `discount_to_all_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_to_all_type` decimal(10, 4) NULL DEFAULT NULL,
  `tot_discount_to_all_amt` decimal(10, 4) NULL DEFAULT NULL,
  `subtotal` decimal(9, 2) NULL DEFAULT NULL,
  `round_off` decimal(9, 2) NULL DEFAULT NULL,
  `paid_amount` decimal(9, 2) NULL DEFAULT NULL,
  `grand_total` decimal(9, 2) NULL DEFAULT NULL,
  `return_bit` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_note` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pos` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_status` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_salesitems
-- ----------------------------
DROP TABLE IF EXISTS `db_salesitems`;
CREATE TABLE `db_salesitems`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_qty` decimal(10, 4) NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `sales_status` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price_per_unit` decimal(9, 2) NULL DEFAULT NULL,
  `tax_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_id` int(11) NULL DEFAULT NULL,
  `tax_amt` decimal(9, 2) NULL DEFAULT NULL,
  `discount_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_type` decimal(10, 4) NULL DEFAULT NULL,
  `discount_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_salesitemsreturn
-- ----------------------------
DROP TABLE IF EXISTS `db_salesitemsreturn`;
CREATE TABLE `db_salesitemsreturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `return_status` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `price_per_unit` decimal(9, 2) NULL DEFAULT NULL,
  `tax_type` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_id` int(11) NULL DEFAULT NULL,
  `tax_amt` decimal(9, 2) NULL DEFAULT NULL,
  `discount_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_type` decimal(10, 4) NULL DEFAULT NULL,
  `discount_amt` decimal(9, 2) NULL DEFAULT NULL,
  `unit_total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `total_cost` decimal(9, 2) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `return_id` int(11) NULL DEFAULT NULL,
  `return_qty` decimal(10, 4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_salespayments
-- ----------------------------
DROP TABLE IF EXISTS `db_salespayments`;
CREATE TABLE `db_salespayments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  `payment_type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment` decimal(9, 2) NULL DEFAULT NULL,
  `payment_note` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_salespaymentsreturn
-- ----------------------------
DROP TABLE IF EXISTS `db_salespaymentsreturn`;
CREATE TABLE `db_salespaymentsreturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NULL DEFAULT NULL,
  `return_id` int(11) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  `payment_type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment` decimal(9, 2) NULL DEFAULT NULL,
  `payment_note` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_salesreturn
-- ----------------------------
DROP TABLE IF EXISTS `db_salesreturn`;
CREATE TABLE `db_salesreturn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `return_code` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reference_no` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `return_date` date NULL DEFAULT NULL,
  `return_status` int(4) NULL DEFAULT NULL,
  `customer_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_charges_input` decimal(9, 2) NULL DEFAULT NULL,
  `other_charges_tax_id` int(5) NULL DEFAULT NULL,
  `other_charges_amt` decimal(9, 2) NULL DEFAULT NULL,
  `discount_to_all_input` decimal(10, 4) NULL DEFAULT NULL,
  `discount_to_all_type` decimal(10, 4) NULL DEFAULT NULL,
  `tot_discount_to_all_amt` decimal(10, 4) NULL DEFAULT NULL,
  `subtotal` decimal(9, 2) NULL DEFAULT NULL,
  `round_off` decimal(9, 2) NULL DEFAULT NULL,
  `paid_amount` decimal(9, 2) NULL DEFAULT NULL,
  `grand_total` decimal(9, 2) NULL DEFAULT NULL,
  `return_note` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_id` int(11) NULL DEFAULT NULL,
  `pos` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_sitesettings
-- ----------------------------
DROP TABLE IF EXISTS `db_sitesettings`;
CREATE TABLE `db_sitesettings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency_id` int(11) NULL DEFAULT NULL,
  `currency_placement` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `language_id` int(11) NULL DEFAULT NULL,
  `timezone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_format` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time_format` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_discount` decimal(10, 4) NULL DEFAULT NULL,
  `change_return` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sales_invoice_format_id` int(11) NULL DEFAULT NULL,
  `sales_invoice_footer_text` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `version` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `domain` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `round_off` decimal(10, 4) NULL DEFAULT NULL,
  `show_upi_code` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_sitesettings
-- ----------------------------
INSERT INTO `db_sitesettings` VALUES (1, 'Cash Register', NULL, 1, '1', 1, 'Africa/Accra', 'Y-m-d', '24', 10.0000, '10', 1, 'Powered By CashRegister.net', '1.1', 'disk', 10.0000, NULL, '1');

-- ----------------------------
-- Table structure for db_smsapi
-- ----------------------------
DROP TABLE IF EXISTS `db_smsapi`;
CREATE TABLE `db_smsapi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `key` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `key_val_` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sms_status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_smstemplates
-- ----------------------------
DROP TABLE IF EXISTS `db_smstemplates`;
CREATE TABLE `db_smstemplates`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `content` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `variables` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `undelete_bit` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_sobpayments
-- ----------------------------
DROP TABLE IF EXISTS `db_sobpayments`;
CREATE TABLE `db_sobpayments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NULL DEFAULT NULL,
  `payment_type` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment` int(15) NULL DEFAULT NULL,
  `payment_note` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_time` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `system_ip` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for db_states
-- ----------------------------
DROP TABLE IF EXISTS `db_states`;
CREATE TABLE `db_states`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_states
-- ----------------------------
INSERT INTO `db_states` VALUES (1, 'Accra', '1', '1');

-- ----------------------------
-- Table structure for db_stockentry
-- ----------------------------
DROP TABLE IF EXISTS `db_stockentry`;
CREATE TABLE `db_stockentry`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `entry_date` date NULL DEFAULT NULL,
  `item_id` int(10) NULL DEFAULT NULL,
  `qty` int(10) NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_stockentry
-- ----------------------------
INSERT INTO `db_stockentry` VALUES (3, '2021-02-07', 1, 30, '0');
INSERT INTO `db_stockentry` VALUES (4, '2021-02-07', 2, 30, '0');

-- ----------------------------
-- Table structure for db_suppliers
-- ----------------------------
DROP TABLE IF EXISTS `db_suppliers`;
CREATE TABLE `db_suppliers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_code` int(11) NULL DEFAULT NULL,
  `supplier_name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` int(15) NULL DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `state_id` int(11) NULL DEFAULT NULL,
  `city` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `purchase_due` timestamp(0) NULL DEFAULT NULL,
  `purchase_return_due` timestamp(0) NULL DEFAULT NULL,
  `postcode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `opening_balance` decimal(9, 2) NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gstin` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax_number` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_suppliers
-- ----------------------------
INSERT INTO `db_suppliers` VALUES (1, 1, 'Kasapreko', 241763214, '0241763214', 'emodatt08@gmail.com', 1, 1, 'Adenta', NULL, NULL, '233', 'P.O.Box AH 445,  Adenta-Accra', 100.00, '::1', 'MSI', '2021-02-03', '09:05:58', 'emodatt08', '', 'P099373545', '0');

-- ----------------------------
-- Table structure for db_tax
-- ----------------------------
DROP TABLE IF EXISTS `db_tax`;
CREATE TABLE `db_tax`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tax` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `group_bit` int(15) NULL DEFAULT NULL,
  `subtax_ids` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_tax
-- ----------------------------
INSERT INTO `db_tax` VALUES (1, 'VAT', '3', NULL, NULL, '1');

-- ----------------------------
-- Table structure for db_units
-- ----------------------------
DROP TABLE IF EXISTS `db_units`;
CREATE TABLE `db_units`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_units
-- ----------------------------
INSERT INTO `db_units` VALUES (1, '3', '# units', '1');

-- ----------------------------
-- Table structure for db_users
-- ----------------------------
DROP TABLE IF EXISTS `db_users`;
CREATE TABLE `db_users`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` int(16) NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(2) NULL DEFAULT NULL,
  `created_date` date NULL DEFAULT NULL,
  `created_time` time(0) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `system_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NULL DEFAULT NULL,
  `system_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `profile_picture` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_email`(`username`, `mobile`, `email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of db_users
-- ----------------------------
INSERT INTO `db_users` VALUES (1, 'emodatt08', '5f4dcc3b5aa765d61d8327deb882cf99', 241763214, 'emodatt08@gmail.com', 1, '2021-01-22', '07:47:23', 'emodatt08', '127.0.0.1', 'MacR', NULL, '0');
INSERT INTO `db_users` VALUES (2, 'james', '5f4dcc3b5aa765d61d8327deb882cf99', 241763214, 'afrojive7@gmail.com', 2, '2021-01-22', '07:47:23', 'emodatt08', '127.0.0.1', 'jamooo', NULL, '0');
INSERT INTO `db_users` VALUES (3, 'enochi', '5f4dcc3b5aa765d61d8327deb882cf99', 241763214, 'missenochi@gmail.com', 3, '2021-01-22', '07:47:23', 'emodatt08', '127.0.0.1', 'EnoChiHillbert', NULL, '0');

-- ----------------------------
-- Table structure for db_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `db_warehouse`;
CREATE TABLE `db_warehouse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `website` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `location` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `state_id` int(11) NULL DEFAULT NULL,
  `city` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
